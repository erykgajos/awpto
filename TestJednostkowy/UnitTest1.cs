﻿using System;
using Awpto;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestJednostkowy
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int wynik = Kalkulator.Dodaj(2, 2);
            Assert.AreEqual(wynik, 4);
            
        }

        [TestMethod]
        public void TestMethod2()
        {
            int wynik = Kalkulator.Dodaj(2, 2);
            Assert.AreNotEqual(wynik, 3);

        }
    }
}
